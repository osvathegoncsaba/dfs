Depth First Search simulation with graphical representation

build: javac DrawDFS.java 

Note: Node.java and JGraph.java are required in the same folder as DrawDFS.java for DrawDFS.java to build

Usage: java DrawDFS <input_filename (contains n then n*n ones or zeros)> <starting_point (or 0 for all components)> <delay_time_in_microseconds (the amount of time the DFS stays at each node)

Note: input file is required to be in the folder where DrawDFS.class is before executing.