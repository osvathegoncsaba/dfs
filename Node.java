/*
    @Author Osvath Egon-Csaba
    contact: osvathegoncsaba@gmail.com
    Depth First Search simulation
    No rights reserved. Use it as you wish ;)
 */

import java.awt.*;

public class Node {
    private int x,y,circleSize;

    public Node(double x, double y, int i, Color color, int circeSize) {
        this.x=(int)x;
        this.y=(int)y;
        number=i;
        this.color=color;
        this.circleSize=circeSize;
    }

    public int getCircleSize() {
        return circleSize;
    }

    public char[] getNumberAsCharArray()
    {
        return String.valueOf(number).toCharArray();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    private Color color;

    private int number;
}
