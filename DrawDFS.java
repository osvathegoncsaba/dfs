/*
    @Author Osvath Egon-Csaba
    contact: osvathegoncsaba@gmail.com
    Depth First Search simulation
    No rights reserved. Use it as you wish ;)
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class DrawDFS extends JFrame implements ComponentListener {
    private int graph[][], n, width, height, circleSize=50;
    private Node nodes[];
    private JGraph jGraph;
    private JButton jbutton;

    public DrawDFS(String filename, int width, int height, int startingPoint, int delayTime)
    {
        this.width=width;
        this.height=height;
        setDefaultLookAndFeelDecorated(true);
        setSize(width,height);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(true);
        setTitle("Depth First Search");
        setBackground(Color.WHITE);
        addComponentListener(this);
        try {
            //Read input file and store the graph
            Scanner scanner = new Scanner(new File(filename));
            n=scanner.nextInt();
            graph = new int[n][n];
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    graph[i][j]=scanner.nextInt();
                }
            }
            //Create nodes by calculating their coordinates
            nodes = new Node[n];
            for (int i = 0; i < n; i++) {
                nodes[i] = new Node((width >> 1) +Math.cos(Math.PI/2-(double)i*2*Math.PI/n)*(width>>1)*0.75 - (circleSize >> 1), (height >> 1) -Math.sin(Math.PI/2-(double)i*2*Math.PI/n)*(height>>1)*0.75- (circleSize >> 1),i+1, Color.WHITE,circleSize);
            }
            //Draw them
            jbutton=new JButton("Start");
            jbutton.addActionListener(e -> {
                if(startingPoint==0)
                {
                    //because otherwise it would block the GUI from repainting this method needs to be threaded
                    new Thread(()-> {
                        int counter=1;
                        for (int i = 0; i < graph.length; i++) {
                            final int k=i;
                            if(nodes[i].getColor()==Color.WHITE)
                            {
                                    System.out.print(counter++ +". ");
                                    DFS(k,graph,nodes,delayTime);
                                    System.out.println();
                            }
                        }
                    }).start();
                }else
                new Thread(() -> {
                    //because otherwise it would block the GUI from repainting this method needs to be threaded
                    try {
                        DFS(startingPoint-1,graph,nodes,delayTime);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }).start();
            });
            add(jbutton,BorderLayout.SOUTH);
            paint();
            setVisible(true);
            ActionListener taskPerformer = evt -> repaint();
            new Timer((startingPoint>>2), taskPerformer).start();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    //Recursive Depth First Search Algorithm coloring the Nodes of the Graph
    private void DFS(int startingPoint, int[][] graph, Node[] nodes, int delayTime) {
        nodes[startingPoint].setColor(Color.GREEN);
        System.out.print((startingPoint+1)+" ");
        repaint();
        try {
            TimeUnit.MILLISECONDS.sleep(delayTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < graph.length; i++) {
            if(graph[startingPoint][i]==1 && nodes[i].getColor()==Color.WHITE)
            {
                for (int j = 0; j < graph.length; j++) {
                    if(graph[j][startingPoint]==3)
                    {
                        graph[j][startingPoint]=graph[startingPoint][j]=2;
                    }
                }
                graph[startingPoint][i]=graph[i][startingPoint]=3;
                nodes[startingPoint].setColor(Color.RED);
                DFS(i,graph,nodes,delayTime);
                nodes[i].setColor(Color.RED);
                graph[startingPoint][i]=graph[i][startingPoint]=2;
            }
        }
        nodes[startingPoint].setColor(Color.RED);
    }

    public void paint(){
        jGraph = new JGraph(width,height,nodes,graph);
        add(jGraph);
        jGraph.setVisible(true);
    }

    @Override
    public void componentResized(ComponentEvent e) {
        width=getWidth();
        height=getHeight();
        for (int i = 0; i < n; i++) {
            nodes[i] = new Node((width >> 1) +Math.cos(Math.PI/2-(double)i*2*Math.PI/n)*(width>>1)*0.75 - (circleSize >> 1), (height >> 1) -Math.sin(Math.PI/2-(double)i*2*Math.PI/n)*(height>>1)*0.75- (circleSize >> 1),i+1, nodes[i].getColor(),circleSize);
        }
        repaint();
    }

    @Override
    public void componentMoved(ComponentEvent e) {

    }

    @Override
    public void componentShown(ComponentEvent e) {

    }

    @Override
    public void componentHidden(ComponentEvent e) {

    }

    public static void main(String[] args) {
        if (args.length==3)
            new DrawDFS(args[0],800,800,Integer.valueOf(args[1]), Integer.valueOf(args[2]));
        else System.out.println("Usage: java DrawDFS <input_filename (contains n then n*n ones or zeros)> <starting_point (or 0 for all components)> <delay_time_in_microseconds (the amount of time the DFS stays at each node)>");
    }

}
