/*
    @Author Osvath Egon-Csaba
    contact: osvathegoncsaba@gmail.com
    Depth First Search simulation
    No rights reserved. Use it as you wish ;)
 */

import javax.swing.*;
import java.awt.*;

public class JGraph extends JPanel {

    private Node nodes[];
    private int graph[][];

    public JGraph(int width, int height, Node nodes[], int graph[][]) {
        // set a preferred size for the custom panel.
        setPreferredSize(new Dimension(width,height));
        this.nodes=nodes;
        this.graph=graph;
    }
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        int circleSize = 50;
        for (int i = 0; i < graph.length; i++) {
            for (int j = i+1; j < graph.length; j++) {
                if(graph[i][j]==1)
                {
                    g.drawLine(nodes[i].getX()+(circleSize>>1),nodes[i].getY()+(circleSize>>1),nodes[j].getX()+(circleSize>>1),nodes[j].getY()+(circleSize>>1));
                }
                else {
                    if (graph[i][j]==2)
                    {
                        g.setColor(Color.RED);
                        g.drawLine(nodes[i].getX()+(circleSize>>1),nodes[i].getY()+(circleSize>>1),nodes[j].getX()+(circleSize>>1),nodes[j].getY()+(circleSize>>1));
                        g.setColor(Color.BLACK);
                    }
                    else
                    {
                        if(graph[i][j]==3)
                        {
                            g.setColor(Color.GREEN);
                            g.drawLine(nodes[i].getX()+(circleSize>>1),nodes[i].getY()+(circleSize>>1),nodes[j].getX()+(circleSize>>1),nodes[j].getY()+(circleSize>>1));
                            g.setColor(Color.BLACK);
                        }
                    }
                }
            }
        }
        for (Node node : nodes) {
            g.setColor(node.getColor());
            g.fillOval(node.getX(), node.getY(), circleSize, circleSize);
            g.setColor(Color.BLACK);
            g.drawOval(node.getX(), node.getY(), circleSize, circleSize);
            g.drawChars(node.getNumberAsCharArray(), 0, node.getNumberAsCharArray().length, node.getX() + (node.getCircleSize()>>1), node.getY() + (node.getCircleSize()>>1));
        }
    }
}
